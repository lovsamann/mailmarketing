//
//  ViewController.swift
//  MailMarketing
//
//  Created by Ann on 4/11/16.
//  Copyright © 2016 Unemployment. All rights reserved.
//

import UIKit
import SideMenu

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - private methods
    
    func setup(){
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewControllerWithIdentifier("LeftMenuNavigationController") as? UISideMenuNavigationController
        //add gesture
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.menuPresentMode = .ViewSlideInOut // style in-out
        SideMenuManager.menuAnimationFadeStrength = 0.0
        SideMenuManager.menuBlurEffectStyle = nil
        SideMenuManager.menuShadowOpacity = 0.0
        SideMenuManager.menuAnimationTransformScaleFactor = 1
        SideMenuManager.menuFadeStatusBar = false
    }
}

